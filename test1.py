#test.py
import pytest 

def add(x, y): 
    return x + y 

def test_function(): 
    assert add(2, 3) == 5
