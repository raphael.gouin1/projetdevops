#write.py
import os

# Récupération du chemin du dossier où est stocké le script
script_dir = os.path.dirname(os.path.abspath(__file__)) 
 
# Création du fichier txt et écriture de contenu
f = open(os.path.join(script_dir, "mon_fichier.txt"), "w") 
f.write("Ceci est un fichier texte généré par un script Python") 
f.close()
